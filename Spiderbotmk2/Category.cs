﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class Category
{
    public string CategoryName { get; set; }
    public List<AmmoBox> AmmoBox { get; set; }
    public List<Caliber> IncludedCalibers { get; set; }

    public Category()
    {
        this.AmmoBox = new List<AmmoBox>();
        this.IncludedCalibers = new List<Caliber>();
    }

}