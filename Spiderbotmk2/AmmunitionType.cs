﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class AmmunitionType
{
    public string Name { get; set; }
    public string Effect { get; set; }
    public string Type { get; set; }

}
