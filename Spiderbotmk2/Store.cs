﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public class Store
{
    public List<string> ShelvedFirearms { get; set; }

    public List<string> ShelvedGrenades { get; set; }

    public List<string> ShelvedGrenadeAmmo { get; set; }

    public List<string> ShelvedRocketAmmo { get; set; }

    public List<string> ShelvedItems { get; set; }

    public Store()
    {
        this.ShelvedFirearms = new List<string>();
        this.ShelvedGrenades = new List<string>();
        this.ShelvedGrenadeAmmo = new List<string>();
        this.ShelvedRocketAmmo = new List<string>();
        this.ShelvedItems = new List<string>();
    }

}

