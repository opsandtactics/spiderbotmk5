﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


    public class Grenades
    {
    public string Name { get; set; }
    public string Damage { get; set; }
    public string DamageType { get; set; }
    public string BlastRadius { get; set; }
    public string Range { get; set; }
    public string Size { get; set; }
    public int Weight { get; set; }
    public double Cost { get; set; }
    public string License { get; set; }
    public string Notes { get; set; }

    public override string ToString()
    {
        System.Text.StringBuilder conversion = new System.Text.StringBuilder();
        conversion.Append(Name + " | Damage: " + Damage + " " + DamageType + " | Blast Radius:" + BlastRadius + " | Range:" + Range + " | Size:" + Size + " | Weight:" + Weight.ToString() + "oz" + " | Cost:" + Cost.ToString() + " WP per grenade"  + " | License:" + License );
        return conversion.ToString();
    }







}

