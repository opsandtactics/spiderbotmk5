﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Web.Script.Serialization;
using System.Threading;
namespace Spiderbotmk5
{
    public partial class MREform : Form
    {
        public MREform()
        {
            InitializeComponent();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            if (singlemre.Checked == true)
            {


                //string directory = "C:/Users/Alex/Dropbox/Gunbot/Gunstore files/Json files/";// Json location at home
                //string directory = "C:/Users/mazycka/Dropbox/Gunbot/Gunstore files/Json files/"; //Json Location at work
                string programdirectory = Directory.GetCurrentDirectory() + "/"; // Directory for installs
                string mrefile = "MREs.json"; // mrefile
                string manufacturerfile = "Ammomakersupper.txt"; // The ammomakers in a text file. No need for a Json for these.
                List<string> MasterAmmoMakers = File.ReadAllLines(programdirectory + manufacturerfile).ToList();
                Dictionary<string, List<string>> items = new Dictionary<string, List<string>>();
                Mre product = new Mre();

                //using (StreamReader r = new StreamReader(directory + "MreComponents.json"))
                using (StreamReader r = new StreamReader(programdirectory + mrefile))
                {
                    string json = r.ReadToEnd();
                    items = new JavaScriptSerializer().Deserialize<Dictionary<string, List<string>>>(json);
                }

                Random roller = new Random();
                Random roller2 = new Random();
                Random roller3 = new Random();
                Random roller4 = new Random();
                Random roller5 = new Random();
                Random roller6 = new Random();
                Random roller7 = new Random();
                Random roller8 = new Random();
                Random roller9 = new Random();
                Random roller10 = new Random();
                Random roller11 = new Random();
                Random roller12 = new Random();
                Random roller13 = new Random();
                Random roller14 = new Random();
                Random roller15 = new Random();
                Random roller16 = new Random();
                Random roller17 = new Random();
                Random roller18 = new Random();
                Random roller19 = new Random();
                // Pick the container
                int totalAmmountContainer = items["container"].Count;
                int containerroll = roller.Next(0, totalAmmountContainer - 1);
                if (items.ContainsKey("container"))
                {
                    string convert = items["container"][containerroll].ToString();
                    product.Container = convert;

                    convert = "";
                }
                // Container for the drink!
                int totalAmmountLContainer = items["lcontainer"].Count;
                int lcontainerroll = roller2.Next(0, totalAmmountLContainer - 1);
                if (items.ContainsKey("lcontainer"))
                {
                    string convert = items["lcontainer"][lcontainerroll].ToString();
                    product.DrinkContainer = convert;

                    convert = "";
                }
                // Pick the Entree
                int selector = roller3.Next(1, 4); // Choses randomly between 1-4 entrees
                int totalAmmountEntree = items["entree"].Count; // How many we got?
                int entreeroll = roller4.Next(0, totalAmmountEntree - 1); //selects one
                if (items.ContainsKey("entree"))
                {
                    while (selector != -1)
                    {
                        string convert = items["entree"][entreeroll].ToString();
                        product.Entree.Add(convert);
                        convert = "";
                        selector--;
                        entreeroll = roller4.Next(0, totalAmmountEntree - 1);
                    }

                }
                selector = 0;
                //Pick the bread? Brrrrread. Bread.
                selector = roller6.Next(1, 3); // Choses randomly between 2-3 Breads
                int totalAmmountBread = items["bread"].Count; // How many we got?
                int breadroll = roller7.Next(0, totalAmmountBread - 1); //selects one. Also Breadroll lol.


                if (items.ContainsKey("bread"))
                {
                    while (selector != -1)
                    {
                        string convert = items["bread"][breadroll].ToString();
                        product.Bread.Add(convert);
                        convert = "";
                        selector--;
                        breadroll = roller7.Next(0, totalAmmountBread - 1);
                    }

                }

                selector = 0;
                selector = roller9.Next(2, 4); // Choses randomly between 2-3 Spreads
                int totalAmmountSpread = items["spread"].Count; // How many we got?
                int spreadroll = roller10.Next(0, totalAmmountSpread - 1); //selects one. Spreadroll!
                if (items.ContainsKey("spread"))
                {
                    while (selector != -1)
                    {
                        string convert = items["spread"][spreadroll].ToString();
                        if (convert == "JELLY" || convert == "JAM" || convert == "CHUTNEY")
                        {
                            spreadroll = roller10.Next(0, items["hardcandy"].Count - 1);
                            string flavortown = items["hardcandy"][spreadroll].ToString();
                            StringBuilder mixer = new StringBuilder();

                            mixer.Append(flavortown + " " + convert);
                            convert = mixer.ToString();
                        }


                        product.Spread.Add(convert);
                        convert = "";
                        selector--;
                        spreadroll = roller10.Next(0, totalAmmountSpread - 1);
                    }

                }
                selector = 0;
                selector = roller13.Next(1, 7); // Choses randomly between 1 and 7 Extas 
                int totalAmmountExtras = items["extra"].Count; // How many we got?
                int extraroll = roller14.Next(0, totalAmmountExtras - 1); //selects one. Etras
                if (items.ContainsKey("extra"))
                {
                    while (selector != -1)
                    {
                        string convert = items["extra"][extraroll].ToString();
                        product.Extras.Add(convert);
                        convert = "";
                        selector--;
                        extraroll = roller14.Next(0, totalAmmountSpread - 1);
                    }

                }
                // Pick the drink
                int totalAmmountDrink = items["drink"].Count;
                int drinkroll = roller16.Next(0, totalAmmountDrink - 1);
                if (items.ContainsKey("drink"))
                {
                    string convert = items["drink"][drinkroll].ToString();
                    if (convert == "JUICE" || convert == "SODA")
                    {
                        drinkroll = roller17.Next(0, items["hardcandy"].Count - 1);
                        string flavortown = items["hardcandy"][drinkroll].ToString();
                        StringBuilder mixer = new StringBuilder();

                        mixer.Append(flavortown + " " + convert);
                        convert = mixer.ToString();
                    }
                    else if (convert == "TEA")
                    {
                        drinkroll = roller17.Next(0, items["tea"].Count - 1);
                        string flavortown = items["tea"][drinkroll].ToString();
                        StringBuilder mixer = new StringBuilder();

                        mixer.Append(flavortown + " " + convert);
                        convert = mixer.ToString();
                    }
                    else if (convert == "MILK")
                    {
                        drinkroll = roller17.Next(0, items["milk"].Count - 1);
                        string flavortown = items["milk"][drinkroll].ToString();
                        StringBuilder mixer = new StringBuilder();

                        mixer.Append(flavortown + " " + convert);
                        convert = mixer.ToString();

                    }
                    product.Drink = convert;
                    convert = "";

                }

                // Pick the Dessert
                int totalAmmountDessert = items["dessert"].Count;
                int dessertroll = roller18.Next(0, totalAmmountDessert - 1);
                if (items.ContainsKey("dessert"))
                {
                    string convert = items["dessert"][dessertroll].ToString();
                    if (convert == "GUM")
                    {
                        dessertroll = roller19.Next(0, items["gum"].Count - 1);
                        string flavortown = items["gum"][dessertroll].ToString();
                        StringBuilder mixer = new StringBuilder();

                        mixer.Append(flavortown + " " + convert);
                        convert = mixer.ToString();
                    }
                    else if (convert == "CRACKERS")
                    {
                        dessertroll = roller19.Next(0, items["crackers"].Count - 1);
                        string flavortown = items["crackers"][dessertroll].ToString();
                        StringBuilder mixer = new StringBuilder();
                        mixer.Append(flavortown + " " + convert);
                        convert = mixer.ToString();
                    }
                    else if (convert == "PIE")
                    {
                        dessertroll = roller19.Next(0, items["pie"].Count - 1);
                        string flavortown = items["pie"][dessertroll].ToString();
                        StringBuilder mixer = new StringBuilder();
                        mixer.Append("A SLICE OF " + flavortown + " " + convert);
                        convert = mixer.ToString();
                    }
                    else if (convert == "PUDDING")
                    {
                        dessertroll = roller19.Next(0, items["pudding"].Count - 1);
                        string flavortown = items["pudding"][dessertroll].ToString();
                        StringBuilder mixer = new StringBuilder();
                        mixer.Append("A CAN OF " + flavortown + " " + convert);
                        convert = mixer.ToString();
                    }
                    else if (convert == "COOKIE" || convert == "COOKIE DOUGH")
                    {
                        dessertroll = roller19.Next(0, items["cookie"].Count - 1);
                        string flavortown = items["cookie"][dessertroll].ToString();
                        StringBuilder mixer = new StringBuilder();
                        mixer.Append("A PACK OF " + flavortown + " " + convert);
                        convert = mixer.ToString();
                    }
                    else if (convert == "ROUNDS")
                    {
                        
                        string ammomaker = MasterAmmoMakers[roller5.Next(0, MasterAmmoMakers.Count - 1)];
                        dessertroll = roller19.Next(0, items["bullets"].Count - 1);
                        string flavortown = items["bullets"][dessertroll].ToString();
                        StringBuilder mixer = new StringBuilder();
                        mixer.Append("A BOX OF " + ammomaker + " BRAND " + flavortown + " " + convert);
                        convert = mixer.ToString();
                    }
                    else if (convert == "SHOTSHELLS")
                    {
                        string ammomaker = MasterAmmoMakers[roller5.Next(0, MasterAmmoMakers.Count - 1)];
                        dessertroll = roller19.Next(0, items["shells"].Count - 1);
                        string flavortown = items["shells"][dessertroll].ToString();
                        StringBuilder mixer = new StringBuilder();
                        mixer.Append("A BOX OF " + ammomaker + " BRAND " + flavortown + " " + convert);
                        convert = mixer.ToString();
                    }
                    else if (convert == "TANK")
                    {
                        dessertroll = roller19.Next(0, items["tank"].Count - 1);
                        string flavortown = items["tank"][dessertroll].ToString();
                        StringBuilder mixer = new StringBuilder();
                        mixer.Append("A MINI COLLECTABLE " + flavortown + " " + convert);
                        convert = mixer.ToString();
                    }
                    else if (convert == "CLEANING KIT AND MANUAL")
                    {
                        dessertroll = roller19.Next(0, items["cleaning"].Count - 1);
                        string flavortown = items["cleaning"][dessertroll].ToString();
                        StringBuilder mixer = new StringBuilder();

                        mixer.Append("A " + flavortown + " " + convert);
                        convert = mixer.ToString();
                    }
                    else if (convert == "GRENADE")
                    {
                        dessertroll = roller19.Next(0, items["grenade"].Count - 1);
                        string flavortown = items["grenade"][dessertroll].ToString();
                        string tastevillage = items["live"][roller3.Next(0, items["live"].Count - 1)];
                        StringBuilder mixer = new StringBuilder();

                        mixer.Append("A " + tastevillage + " " + flavortown + " " + convert);
                        convert = mixer.ToString();
                    }
                    else if (convert == "HARD CANDY" || convert == "LOLLYPOP" || convert == "SOUR CANDY" || convert == "DRIED")
                    {
                        dessertroll = roller19.Next(0, items["hardcandy"].Count - 1);
                        string flavortown = items["hardcandy"][dessertroll].ToString();
                        StringBuilder mixer = new StringBuilder();

                        if (convert == "HARD CANDY" || convert == "SOUR CANDY")
                        {
                            mixer.Append("A PACK OF " + flavortown + " FLAVORED " + convert);

                        }
                        else if (convert == "LOLLYPOP")
                        {
                            mixer.Append("A " + flavortown + " FLAVORED " + convert);

                        }
                        else if (convert == "DRIED")
                        {

                            mixer.Append("A PACK OF " + convert + " " + flavortown + "  FRUIT PIECES");
                        }

                        convert = mixer.ToString();
                    }
                    product.Dessert = convert;
                    convert = "";
                }
                int breadcount = product.Bread.Count - 1;
                int entreecount = product.Entree.Count - 1;
                int spreadcount = product.Spread.Count - 1;
                int extrastatic = product.Extras.Count - 1;
                int extracount = 0;

                StringBuilder breadholder = new StringBuilder();
                StringBuilder entreeholder = new StringBuilder();
                StringBuilder spreadholder = new StringBuilder();
                StringBuilder extraholder = new StringBuilder();

                //Breadbuilder for straight down
                while (breadcount != -1)
                {
                    breadholder.Append(product.Bread[breadcount] + Environment.NewLine);
                    breadcount--;
                }


                // Entree builder. For straight down

                while (entreecount != -1)
                {
                    entreeholder.Append(product.Entree[entreecount] + Environment.NewLine);
                    entreecount--;
                }
                //spreadbuilder. This one goes down in a line

                while (spreadcount != -1)
                {
                    spreadholder.Append(product.Spread[spreadcount] + Environment.NewLine);
                    spreadcount--;
                }

                //Extra count. This one Generates the form with the newlinesd
                while (extracount < extrastatic)
                {
                    if (extracount == extrastatic - 1)
                    {
                        extraholder.Append(product.Extras[extracount]);
                        extracount++;
                    }
                    else
                    {
                        extraholder.Append(product.Extras[extracount] + Environment.NewLine);
                        extracount++;

                    }

                }





                mreoutput.Text = "____________________________________________" + Environment.NewLine + product.Container + Environment.NewLine + Environment.NewLine + "ENTREES:" + Environment.NewLine + entreeholder + Environment.NewLine + "BREAD:" + Environment.NewLine + breadholder + Environment.NewLine + "PACKAGED SPREADS:" + Environment.NewLine + spreadholder + Environment.NewLine + "DRINK:" + Environment.NewLine + product.Drink + " IN A " + product.DrinkContainer + Environment.NewLine + Environment.NewLine + "DESSERT: " + Environment.NewLine + product.Dessert + Environment.NewLine + Environment.NewLine + "ACCESSORIES: " + Environment.NewLine + extraholder + Environment.NewLine + "ONE(1) MRE SPOON" + Environment.NewLine + "ONE(1) FLAMELESS RADIATION HEATER";
            }
            else if (mrebox.Checked == true)
            {
                int Box = 12;
                StringBuilder savetofile = new StringBuilder();
                while (Box != 0)
                {
                    //string directory = "C:/Users/Alex/Dropbox/Gunbot/Gunstore files/Json files/";// Json location at home
                    //string directory = "C:/Users/mazycka/Dropbox/Gunbot/Gunstore files/Json files/"; //Json Location at work
                    string programdirectory = Environment.CurrentDirectory + "/"; // Directory for installs
                    string mrefile = "MREs.json"; // mrefile
                    string manufacturerfile = "Ammomakersupper.txt"; // The ammomakers in a text file. No need for a Json for these.
                    List<string> MasterAmmoMakers = File.ReadAllLines(programdirectory + manufacturerfile).ToList();
                    Dictionary<string, List<string>> items = new Dictionary<string, List<string>>();
                    Mre product = new Mre();

                    //using (StreamReader r = new StreamReader(directory + "MreComponents.json"))
                    using (StreamReader r = new StreamReader(programdirectory + mrefile))
                    {
                        string json = r.ReadToEnd();
                        items = new JavaScriptSerializer().Deserialize<Dictionary<string, List<string>>>(json);
                    }

                    Random roller = new Random();
                    Random roller2 = new Random();
                    Random roller3 = new Random();
                    Random roller4 = new Random();
                    Random roller5 = new Random();
                    Random roller6 = new Random();
                    Random roller7 = new Random();
                    Random roller8 = new Random();
                    Random roller9 = new Random();
                    Random roller10 = new Random();
                    Random roller11 = new Random();
                    Random roller12 = new Random();
                    Random roller13 = new Random();
                    Random roller14 = new Random();
                    Random roller15 = new Random();
                    Random roller16 = new Random();
                    Random roller17 = new Random();
                    Random roller18 = new Random();
                    Random roller19 = new Random();
                    // Pick the container
                    int totalAmmountContainer = items["container"].Count;
                    int containerroll = roller.Next(0, totalAmmountContainer - 1);
                    if (items.ContainsKey("container"))
                    {
                        string convert = items["container"][containerroll].ToString();
                        product.Container = convert;

                        convert = "";
                    }
                    // Container for the drink!
                    int totalAmmountLContainer = items["lcontainer"].Count;
                    int lcontainerroll = roller2.Next(0, totalAmmountLContainer - 1);
                    if (items.ContainsKey("lcontainer"))
                    {
                        string convert = items["lcontainer"][lcontainerroll].ToString();
                        product.DrinkContainer = convert;

                        convert = "";
                    }
                    // Pick the Entree
                    int selector = roller3.Next(1, 4); // Choses randomly between 1-4 entrees
                    int totalAmmountEntree = items["entree"].Count; // How many we got?
                    int entreeroll = roller4.Next(0, totalAmmountEntree - 1); //selects one
                    if (items.ContainsKey("entree"))
                    {
                        while (selector != -1)
                        {
                            string convert = items["entree"][entreeroll].ToString();
                            product.Entree.Add(convert);
                            convert = "";
                            selector--;
                            entreeroll = roller4.Next(0, totalAmmountEntree - 1);
                        }

                    }
                    selector = 0;
                    //Pick the bread? Brrrrread. Bread.
                    selector = roller6.Next(1, 3); // Choses randomly between 2-3 Breads
                    int totalAmmountBread = items["bread"].Count; // How many we got?
                    int breadroll = roller7.Next(0, totalAmmountBread - 1); //selects one. Also Breadroll lol.


                    if (items.ContainsKey("bread"))
                    {
                        while (selector != -1)
                        {
                            string convert = items["bread"][breadroll].ToString();
                            product.Bread.Add(convert);
                            convert = "";
                            selector--;
                            breadroll = roller7.Next(0, totalAmmountBread - 1);
                        }

                    }

                    selector = 0;
                    selector = roller9.Next(1, 3); // Choses randomly between 2-3 Spreads
                    int totalAmmountSpread = items["spread"].Count; // How many we got?
                    int spreadroll = roller10.Next(0, totalAmmountSpread - 1); //selects one. Spreadroll!
                    if (items.ContainsKey("spread"))
                    {
                        while (selector != -1)
                        {
                            string convert = items["spread"][spreadroll].ToString();
                            if (convert == "JELLY" || convert == "JAM" || convert == "CHUTNEY")
                            {
                                spreadroll = roller10.Next(0, items["hardcandy"].Count - 1);
                                string flavortown = items["hardcandy"][spreadroll].ToString();
                                StringBuilder mixer = new StringBuilder();

                                mixer.Append(flavortown + " " + convert);
                                convert = mixer.ToString();
                            }


                            product.Spread.Add(convert);
                            convert = "";
                            selector--;
                            spreadroll = roller10.Next(0, totalAmmountSpread - 1);
                        }

                    }
                    selector = 0;
                    selector = roller13.Next(1, 7); // Choses randomly between 1 and 7 Extas 
                    int totalAmmountExtras = items["extra"].Count; // How many we got?
                    int extraroll = roller14.Next(0, totalAmmountExtras - 1); //selects one. Etras
                    if (items.ContainsKey("extra"))
                    {
                        while (selector != -1)
                        {
                            string convert = items["extra"][extraroll].ToString();
                            product.Extras.Add(convert);
                            convert = "";
                            selector--;
                            extraroll = roller14.Next(0, totalAmmountSpread - 1);
                        }

                    }
                    // Pick the drink
                    int totalAmmountDrink = items["drink"].Count;
                    int drinkroll = roller16.Next(0, totalAmmountDrink - 1);
                    if (items.ContainsKey("drink"))
                    {
                        string convert = items["drink"][drinkroll].ToString();
                        if (convert == "JUICE" || convert == "SODA")
                        {
                            drinkroll = roller17.Next(0, items["hardcandy"].Count - 1);
                            string flavortown = items["hardcandy"][drinkroll].ToString();
                            StringBuilder mixer = new StringBuilder();

                            mixer.Append(flavortown + " " + convert);
                            convert = mixer.ToString();
                        }
                        else if (convert == "TEA")
                        {
                            drinkroll = roller17.Next(0, items["tea"].Count - 1);
                            string flavortown = items["tea"][drinkroll].ToString();
                            StringBuilder mixer = new StringBuilder();

                            mixer.Append(flavortown + " " + convert);
                            convert = mixer.ToString();
                        }
                        else if (convert == "MILK")
                        {
                            drinkroll = roller17.Next(0, items["milk"].Count - 1);
                            string flavortown = items["milk"][drinkroll].ToString();
                            StringBuilder mixer = new StringBuilder();

                            mixer.Append(flavortown + " " + convert);
                            convert = mixer.ToString();

                        }
                        product.Drink = convert;
                        convert = "";

                    }

                    // Pick the Dessert
                    int totalAmmountDessert = items["dessert"].Count;
                    int dessertroll = roller18.Next(0, totalAmmountDessert - 1);
                    if (items.ContainsKey("dessert"))
                    {
                        string convert = items["dessert"][dessertroll].ToString();
                        if (convert == "GUM")
                        {
                            dessertroll = roller19.Next(0, items["gum"].Count - 1);
                            string flavortown = items["gum"][dessertroll].ToString();
                            StringBuilder mixer = new StringBuilder();

                            mixer.Append(flavortown + " " + convert);
                            convert = mixer.ToString();
                        }
                        else if (convert == "CRACKERS")
                        {
                            dessertroll = roller19.Next(0, items["crackers"].Count - 1);
                            string flavortown = items["crackers"][dessertroll].ToString();
                            StringBuilder mixer = new StringBuilder();
                            mixer.Append(flavortown + " " + convert);
                            convert = mixer.ToString();
                        }
                        else if (convert == "PIE")
                        {
                            dessertroll = roller19.Next(0, items["pie"].Count - 1);
                            string flavortown = items["pie"][dessertroll].ToString();
                            StringBuilder mixer = new StringBuilder();
                            mixer.Append("A SLICE OF " + flavortown + " " + convert);
                            convert = mixer.ToString();
                        }
                        else if (convert == "PUDDING")
                        {
                            dessertroll = roller19.Next(0, items["pudding"].Count - 1);
                            string flavortown = items["pudding"][dessertroll].ToString();
                            StringBuilder mixer = new StringBuilder();
                            mixer.Append("A CAN OF " + flavortown + " " + convert);
                            convert = mixer.ToString();
                        }
                        else if (convert == "COOKIE" || convert == "COOKIE DOUGH")
                        {
                            dessertroll = roller19.Next(0, items["cookie"].Count - 1);
                            string flavortown = items["cookie"][dessertroll].ToString();
                            StringBuilder mixer = new StringBuilder();
                            mixer.Append("A PACK OF " + flavortown + " " + convert);
                            convert = mixer.ToString();
                        }
                        else if (convert == "ROUNDS")
                        {
                            string ammomaker = MasterAmmoMakers[roller5.Next(0, MasterAmmoMakers.Count - 1)];
                            dessertroll = roller19.Next(0, items["bullets"].Count - 1);
                            string flavortown = items["bullets"][dessertroll].ToString();
                            StringBuilder mixer = new StringBuilder();
                            mixer.Append("A BOX OF " + flavortown + " " + convert);
                            convert = mixer.ToString();
                        }
                        else if (convert == "SHOTSHELLS")
                        {
                            string ammomaker = MasterAmmoMakers[roller5.Next(0, MasterAmmoMakers.Count - 1)];
                            dessertroll = roller19.Next(0, items["shells"].Count - 1);
                            string flavortown = items["shells"][dessertroll].ToString();
                            StringBuilder mixer = new StringBuilder();
                            mixer.Append("A BOX OF " + flavortown + " " + convert);
                            convert = mixer.ToString();
                        }
                        else if (convert == "TANK")
                        {
                            dessertroll = roller19.Next(0, items["tank"].Count - 1);
                            string flavortown = items["tank"][dessertroll].ToString();
                            StringBuilder mixer = new StringBuilder();
                            mixer.Append("A MINI COLLECTABLE " + flavortown + " " + convert);
                            convert = mixer.ToString();
                        }
                        else if (convert == "CLEANING KIT AND MANUAL")
                        {
                            dessertroll = roller19.Next(0, items["cleaning"].Count - 1);
                            string flavortown = items["cleaning"][dessertroll].ToString();
                            StringBuilder mixer = new StringBuilder();

                            mixer.Append("A " + flavortown + " " + convert);
                            convert = mixer.ToString();
                        }
                        else if (convert == "GRENADE")
                        {
                            dessertroll = roller19.Next(0, items["grenade"].Count - 1);
                            string flavortown = items["grenade"][dessertroll].ToString();
                            string tastevillage = items["live"][roller3.Next(0, items["live"].Count - 1)];
                            StringBuilder mixer = new StringBuilder();

                            mixer.Append("A " + tastevillage + " " + flavortown + " " + convert);
                            convert = mixer.ToString();
                        }
                        else if (convert == "HARD CANDY" || convert == "LOLLYPOP" || convert == "SOUR CANDY" || convert == "DRIED")
                        {
                            dessertroll = roller19.Next(0, items["hardcandy"].Count - 1);
                            string flavortown = items["hardcandy"][dessertroll].ToString();
                            StringBuilder mixer = new StringBuilder();

                            if (convert == "HARD CANDY" || convert == "SOUR CANDY")
                            {
                                mixer.Append("A PACK OF " + flavortown + " FLAVORED " + convert);

                            }
                            else if (convert == "LOLLYPOP")
                            {
                                mixer.Append("A " + flavortown + " FLAVORED " + convert);

                            }
                            else if (convert == "DRIED")
                            {

                                mixer.Append("A PACK OF " + convert + " " + flavortown + "  FRUIT PIECES");
                            }

                            convert = mixer.ToString();
                        }
                        product.Dessert = convert;
                        convert = "";
                    }
                    int breadcount = product.Bread.Count - 1;
                    int entreecount = product.Entree.Count - 1;
                    int spreadcount = product.Spread.Count - 1;
                    int extrastatic = product.Extras.Count - 1;
                    int extracount = 0;

                    StringBuilder breadholder = new StringBuilder();
                    StringBuilder entreeholder = new StringBuilder();
                    StringBuilder spreadholder = new StringBuilder();
                    StringBuilder extraholder = new StringBuilder();

                    //Breadbuilder for straight down
                    while (breadcount != -1)
                    {
                        breadholder.Append(product.Bread[breadcount] + Environment.NewLine);
                        breadcount--;
                    }


                    // Entree builder. For straight down

                    while (entreecount != -1)
                    {
                        entreeholder.Append(product.Entree[entreecount] + Environment.NewLine);
                        entreecount--;
                    }
                    //spreadbuilder. This one goes down in a line

                    while (spreadcount != -1)
                    {
                        spreadholder.Append(product.Spread[spreadcount] + Environment.NewLine);
                        spreadcount--;
                    }

                    //Extra count. This one Generates the form with the newlinesd
                    while (extracount < extrastatic)
                    {
                        if (extracount == extrastatic - 1)
                        {
                            extraholder.Append(product.Extras[extracount]);
                            extracount++;
                        }
                        else
                        {
                            extraholder.Append(product.Extras[extracount] + Environment.NewLine);
                            extracount++;

                        }

                    }





                    string outputter = "____________________________________________" + Environment.NewLine + product.Container + Environment.NewLine + Environment.NewLine + "ENTREES:" + Environment.NewLine + entreeholder + Environment.NewLine + "BREAD:" + Environment.NewLine + breadholder + Environment.NewLine + "PACKAGED SPREADS:" + Environment.NewLine + spreadholder + Environment.NewLine + "DRINK:" + Environment.NewLine + product.Drink + " IN A " + product.DrinkContainer + Environment.NewLine + Environment.NewLine + "DESSERT: " + Environment.NewLine + product.Dessert + Environment.NewLine + Environment.NewLine + "ACCESSORIES: " + Environment.NewLine + extraholder + Environment.NewLine + "ONE(1) MRE SPOON" + Environment.NewLine + "ONE(1) FLAMELESS RADIATION HEATER";
                    savetofile.Append(outputter + Environment.NewLine + Environment.NewLine);
                    Thread.Sleep(100);
                    Box--;

                }
                SaveFileDialog SaveMREDialog = new SaveFileDialog();
                SaveMREDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                SaveMREDialog.FileName = "NewMRE.txt";
                SaveMREDialog.RestoreDirectory = true;

                if (SaveMREDialog.ShowDialog() == DialogResult.OK)
                {
                    using (StreamWriter newstream = new StreamWriter(SaveMREDialog.FileName))
                    {

                        newstream.Write(savetofile);
                        newstream.Close();
                    }
                }

            }
        }

        private void singlemre_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void saveMREToolStripMenuItem_Click(object sender, EventArgs e)
        {

            SaveFileDialog SaveMREDialog = new SaveFileDialog();
            SaveMREDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            SaveMREDialog.FileName = "NewMRE.txt";
            SaveMREDialog.RestoreDirectory = true;

            if (SaveMREDialog.ShowDialog() == DialogResult.OK)
            {
                using (StreamWriter newstream = new StreamWriter(SaveMREDialog.FileName))
                {

                    newstream.Write(mreoutput.Text);
                    newstream.Close();
                }
            }

            /*
            System.Text.StringBuilder messageBoxCS = new System.Text.StringBuilder();
            messageBoxCS.AppendFormat("{0} = {1}", "ClickedItem", e.ToString());
            messageBoxCS.AppendLine();
            MessageBox.Show(messageBoxCS.ToString(), "ItemClicked Event");
            */
        }

        private void mrebox_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void fileToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to quit, Sempai?", "Exit", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {

                Application.Exit();

            }
        }
    }
}