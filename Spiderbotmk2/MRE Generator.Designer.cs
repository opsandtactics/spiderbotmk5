﻿namespace Spiderbotmk5
{
    partial class MREform
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MREform));
            this.gen = new System.Windows.Forms.Button();
            this.mreoutput = new System.Windows.Forms.TextBox();
            this.singlemre = new System.Windows.Forms.RadioButton();
            this.mrebox = new System.Windows.Forms.RadioButton();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveMREToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // gen
            // 
            this.gen.Font = new System.Drawing.Font("Courier New", 7.8F);
            this.gen.Location = new System.Drawing.Point(61, 814);
            this.gen.Margin = new System.Windows.Forms.Padding(4);
            this.gen.Name = "gen";
            this.gen.Size = new System.Drawing.Size(208, 46);
            this.gen.TabIndex = 0;
            this.gen.Text = "Receive MRE";
            this.gen.UseVisualStyleBackColor = true;
            this.gen.Click += new System.EventHandler(this.button1_Click);
            // 
            // mreoutput
            // 
            this.mreoutput.BackColor = System.Drawing.Color.White;
            this.mreoutput.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mreoutput.Location = new System.Drawing.Point(16, 33);
            this.mreoutput.Margin = new System.Windows.Forms.Padding(4);
            this.mreoutput.Multiline = true;
            this.mreoutput.Name = "mreoutput";
            this.mreoutput.ReadOnly = true;
            this.mreoutput.Size = new System.Drawing.Size(453, 772);
            this.mreoutput.TabIndex = 1;
            // 
            // singlemre
            // 
            this.singlemre.AutoSize = true;
            this.singlemre.Checked = true;
            this.singlemre.Font = new System.Drawing.Font("Courier New", 7.8F);
            this.singlemre.ForeColor = System.Drawing.Color.Black;
            this.singlemre.Location = new System.Drawing.Point(296, 814);
            this.singlemre.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.singlemre.Name = "singlemre";
            this.singlemre.Size = new System.Drawing.Size(125, 21);
            this.singlemre.TabIndex = 2;
            this.singlemre.TabStop = true;
            this.singlemre.Text = "Singular MRE";
            this.singlemre.UseVisualStyleBackColor = true;
            this.singlemre.CheckedChanged += new System.EventHandler(this.singlemre_CheckedChanged);
            // 
            // mrebox
            // 
            this.mrebox.AutoSize = true;
            this.mrebox.Font = new System.Drawing.Font("Courier New", 7.8F);
            this.mrebox.Location = new System.Drawing.Point(296, 839);
            this.mrebox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.mrebox.Name = "mrebox";
            this.mrebox.Size = new System.Drawing.Size(149, 21);
            this.mrebox.TabIndex = 3;
            this.mrebox.Text = "Pack of 12 MREs";
            this.mrebox.UseVisualStyleBackColor = true;
            this.mrebox.CheckedChanged += new System.EventHandler(this.mrebox_CheckedChanged);
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(5, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(492, 25);
            this.menuStrip1.TabIndex = 4;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveMREToolStripMenuItem,
            this.toolStripSeparator1,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Font = new System.Drawing.Font("Courier New", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(52, 21);
            this.fileToolStripMenuItem.Text = "File";
            this.fileToolStripMenuItem.Click += new System.EventHandler(this.fileToolStripMenuItem_Click);
            // 
            // saveMREToolStripMenuItem
            // 
            this.saveMREToolStripMenuItem.Name = "saveMREToolStripMenuItem";
            this.saveMREToolStripMenuItem.Size = new System.Drawing.Size(170, 26);
            this.saveMREToolStripMenuItem.Text = "Save MRE...";
            this.saveMREToolStripMenuItem.Click += new System.EventHandler(this.saveMREToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(167, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(170, 26);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // MREform
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(492, 873);
            this.Controls.Add(this.mrebox);
            this.Controls.Add(this.singlemre);
            this.Controls.Add(this.mreoutput);
            this.Controls.Add(this.gen);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Courier New", 7.8F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "MREform";
            this.Text = "MRE Generator";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button gen;
        private System.Windows.Forms.TextBox mreoutput;
        private System.Windows.Forms.RadioButton singlemre;
        private System.Windows.Forms.RadioButton mrebox;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveMREToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
    }
}

