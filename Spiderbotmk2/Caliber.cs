﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class Caliber
{
    public string Name { get; set; }
    public int DiceSize { get; set; }
    public int DiceAmount { get; set; }
    public int DiceModifier { get; set; }
    public string AmmunitionGroup { get; set; }
    public int Recoil { get; set; }
    public int CraftCategory { get; set; }

    public Caliber(string csvLine)
    {

        using (Microsoft.VisualBasic.FileIO.TextFieldParser parser = new Microsoft.VisualBasic.FileIO.TextFieldParser(new StringReader(csvLine)))
        {
            parser.TextFieldType = Microsoft.VisualBasic.FileIO.FieldType.Delimited;
            parser.HasFieldsEnclosedInQuotes = true;
            parser.Delimiters = new string[] { "," };

            string[] values;
            while (!parser.EndOfData)
            {
                values = parser.ReadFields();
                Name = values[0];
                DiceSize = Convert.ToInt32(values[1]);
                DiceAmount = Convert.ToInt32(values[2]);
                DiceModifier = Convert.ToInt32(values[3]);
                AmmunitionGroup = values[4];
                Recoil = Convert.ToInt32(values[5]);
                CraftCategory = Convert.ToInt32(values[6]);
            }
        }


}
    public Caliber()
    {
    }
}
