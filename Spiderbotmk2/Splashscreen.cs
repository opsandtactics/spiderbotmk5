﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace spiderbotmk5
{
    public partial class Splashscreen : Form
    {
        public Splashscreen()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            Gunstoreform newstore = new Gunstoreform();
            newstore.Closed += (s, args) => this.Close();
            newstore.Show();

            
        }

        private void Splashscreen_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Spiderbotmk5.MREform newstore = new Spiderbotmk5.MREform();
            newstore.Closed += (s, args) => this.Close();
            newstore.Show();
            

        }
    }
}
