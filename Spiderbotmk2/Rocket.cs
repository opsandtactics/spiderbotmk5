﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class Rocket
{
    public string Name { get; set; }
    public string Damage { get; set; }
    public string Magazine { get; set; }
    public string Upgrade { get; set; }
    public string RateofFire { get; set; }
    public string Size { get; set; }
    public int Weight { get; set; }
    public double Cost { get; set; }
    public double Error { get; set; }
    public string License { get; set; }
    public string Notes { get; set; }

    public Rocket(string csvLine)
    {

        using (Microsoft.VisualBasic.FileIO.TextFieldParser parser = new Microsoft.VisualBasic.FileIO.TextFieldParser(new StringReader(csvLine)))
        {
            parser.TextFieldType = Microsoft.VisualBasic.FileIO.FieldType.Delimited;
            parser.HasFieldsEnclosedInQuotes = true;
            parser.Delimiters = new string[] { "," };

            string[] values;
            while (!parser.EndOfData)
            {
                values = parser.ReadFields();
                Name = values[0];
                Damage = values[1];
                Magazine = values[2];
                Upgrade = values[3];
                RateofFire = values[5];
                Size = values[6];
                Weight = Convert.ToInt32(values[7]);
                Cost = Convert.ToDouble(values[8]);
                Error = Convert.ToDouble(values[9]);
                License = values[10];
                Notes = values[11];


            }
        }


    }

    public Rocket()
    {

    }

    public override string ToString()
    {
        System.Text.StringBuilder conversion = new System.Text.StringBuilder();
        conversion.Append(Name + " | Damage: " + Damage + " | Magazine:" + Magazine + " | Upgrade Points:" + Upgrade + " | ROF:" + RateofFire + " | Size:" + Size + " | Weight:" + Weight.ToString() + "oz" + " | Cost:" + Cost.ToString() + " WP" + " | Error Range:" + Error.ToString() + "%" + " | License:" + License + Environment.NewLine + "Standard Equipment:" + Notes);
        return conversion.ToString();
    }

}