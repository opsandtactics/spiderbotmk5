﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace spiderbotmk5
{
    class GrenadeLauncher
    {
        public string Name { get; set; }
        public string Caliber { get; set; }
        public string Magazine { get; set; }
        public string Upgrade { get; set; }
        public string Range { get; set; }
        public string RateofFire { get; set; }
        public string Size { get; set; }
        public int Weight { get; set; }
        public double Cost { get; set; }
        public double Error { get; set; }
        public string License { get; set; }
        public string Notes { get; set; }

        public GrenadeLauncher(string csvLine)
        {

            using (Microsoft.VisualBasic.FileIO.TextFieldParser parser = new Microsoft.VisualBasic.FileIO.TextFieldParser(new StringReader(csvLine)))
            {
                parser.TextFieldType = Microsoft.VisualBasic.FileIO.FieldType.Delimited;
                parser.HasFieldsEnclosedInQuotes = true;
                parser.Delimiters = new string[] { "," };

                string[] values;
                while (!parser.EndOfData)
                {
                    values = parser.ReadFields();
                    Name = values[0];
                    Caliber = values[1];
                    Magazine = values[2];
                    Upgrade = values[3];
                    Range = values[4];
                    RateofFire = values[5];
                    Size = values[6];
                    Weight = Convert.ToInt32(values[7]);
                    Cost = Convert.ToDouble(values[8]);
                    Error = Convert.ToDouble(values[9]);
                    License = values[10];
                    Notes = values[11];


                }
            }


        }

        public GrenadeLauncher()
        {
        }
    }
}
