﻿namespace spiderbotmk5
{
    partial class Gunstoreform
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Gunstoreform));
            this.gunstoreoutput = new System.Windows.Forms.TextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.storetabs = new System.Windows.Forms.TabControl();
            this.rangedtab = new System.Windows.Forms.TabPage();
            this.AmmunitionComboBox = new System.Windows.Forms.ComboBox();
            this.GrenadeLauncherComboBox = new System.Windows.Forms.ComboBox();
            this.RocketLauncherComboBox = new System.Windows.Forms.ComboBox();
            this.MachineGunComboBox = new System.Windows.Forms.ComboBox();
            this.PDWComboBox = new System.Windows.Forms.ComboBox();
            this.RifleComboBox = new System.Windows.Forms.ComboBox();
            this.ShotgunComboBox = new System.Windows.Forms.ComboBox();
            this.HandgunComboBox = new System.Windows.Forms.ComboBox();
            this.RestrictionLevelComboBox = new System.Windows.Forms.ComboBox();
            this.WealthLevelComboBox = new System.Windows.Forms.ComboBox();
            this.aacheck = new System.Windows.Forms.CheckBox();
            this.label33 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.induscheck = new System.Windows.Forms.CheckBox();
            this.lateinducheck = new System.Windows.Forms.CheckBox();
            this.earlymodcheck = new System.Windows.Forms.CheckBox();
            this.GrenadeCheckbox = new System.Windows.Forms.CheckBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.button2 = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            this.storetabs.SuspendLayout();
            this.rangedtab.SuspendLayout();
            this.SuspendLayout();
            // 
            // gunstoreoutput
            // 
            this.gunstoreoutput.BackColor = System.Drawing.Color.White;
            this.gunstoreoutput.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gunstoreoutput.Location = new System.Drawing.Point(11, 26);
            this.gunstoreoutput.Margin = new System.Windows.Forms.Padding(2);
            this.gunstoreoutput.Multiline = true;
            this.gunstoreoutput.Name = "gunstoreoutput";
            this.gunstoreoutput.ReadOnly = true;
            this.gunstoreoutput.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.gunstoreoutput.Size = new System.Drawing.Size(1241, 362);
            this.gunstoreoutput.TabIndex = 2;
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1263, 24);
            this.menuStrip1.TabIndex = 43;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveToolStripMenuItem,
            this.toolStripSeparator1,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(47, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.saveToolStripMenuItem.Text = "Save Gunstore...";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(183, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // storetabs
            // 
            this.storetabs.AccessibleName = "storetab";
            this.storetabs.Controls.Add(this.rangedtab);
            this.storetabs.Controls.Add(this.tabPage2);
            this.storetabs.Location = new System.Drawing.Point(12, 393);
            this.storetabs.Name = "storetabs";
            this.storetabs.SelectedIndex = 0;
            this.storetabs.Size = new System.Drawing.Size(1132, 154);
            this.storetabs.TabIndex = 63;
            // 
            // rangedtab
            // 
            this.rangedtab.Controls.Add(this.AmmunitionComboBox);
            this.rangedtab.Controls.Add(this.GrenadeLauncherComboBox);
            this.rangedtab.Controls.Add(this.RocketLauncherComboBox);
            this.rangedtab.Controls.Add(this.MachineGunComboBox);
            this.rangedtab.Controls.Add(this.PDWComboBox);
            this.rangedtab.Controls.Add(this.RifleComboBox);
            this.rangedtab.Controls.Add(this.ShotgunComboBox);
            this.rangedtab.Controls.Add(this.HandgunComboBox);
            this.rangedtab.Controls.Add(this.RestrictionLevelComboBox);
            this.rangedtab.Controls.Add(this.WealthLevelComboBox);
            this.rangedtab.Controls.Add(this.aacheck);
            this.rangedtab.Controls.Add(this.label33);
            this.rangedtab.Controls.Add(this.label30);
            this.rangedtab.Controls.Add(this.induscheck);
            this.rangedtab.Controls.Add(this.lateinducheck);
            this.rangedtab.Controls.Add(this.earlymodcheck);
            this.rangedtab.Controls.Add(this.GrenadeCheckbox);
            this.rangedtab.Controls.Add(this.label28);
            this.rangedtab.Controls.Add(this.label25);
            this.rangedtab.Controls.Add(this.label22);
            this.rangedtab.Controls.Add(this.label19);
            this.rangedtab.Controls.Add(this.label14);
            this.rangedtab.Controls.Add(this.label6);
            this.rangedtab.Controls.Add(this.label5);
            this.rangedtab.Controls.Add(this.label1);
            this.rangedtab.Location = new System.Drawing.Point(4, 22);
            this.rangedtab.Name = "rangedtab";
            this.rangedtab.Padding = new System.Windows.Forms.Padding(3);
            this.rangedtab.Size = new System.Drawing.Size(1124, 128);
            this.rangedtab.TabIndex = 0;
            this.rangedtab.Text = "Ranged Weapons";
            this.rangedtab.UseVisualStyleBackColor = true;
            this.rangedtab.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // AmmunitionComboBox
            // 
            this.AmmunitionComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.AmmunitionComboBox.FormattingEnabled = true;
            this.AmmunitionComboBox.Items.AddRange(new object[] {
            "None",
            "Very Low",
            "Low",
            "Medium",
            "High",
            "Very High",
            "Extremely High"});
            this.AmmunitionComboBox.Location = new System.Drawing.Point(495, 67);
            this.AmmunitionComboBox.Name = "AmmunitionComboBox";
            this.AmmunitionComboBox.Size = new System.Drawing.Size(121, 21);
            this.AmmunitionComboBox.TabIndex = 88;
            // 
            // GrenadeLauncherComboBox
            // 
            this.GrenadeLauncherComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.GrenadeLauncherComboBox.FormattingEnabled = true;
            this.GrenadeLauncherComboBox.Items.AddRange(new object[] {
            "None",
            "Very Low",
            "Low",
            "Medium",
            "High",
            "Very High",
            "Extremely High"});
            this.GrenadeLauncherComboBox.Location = new System.Drawing.Point(495, 26);
            this.GrenadeLauncherComboBox.Name = "GrenadeLauncherComboBox";
            this.GrenadeLauncherComboBox.Size = new System.Drawing.Size(121, 21);
            this.GrenadeLauncherComboBox.TabIndex = 87;
            // 
            // RocketLauncherComboBox
            // 
            this.RocketLauncherComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.RocketLauncherComboBox.FormattingEnabled = true;
            this.RocketLauncherComboBox.Items.AddRange(new object[] {
            "None",
            "Very Low",
            "Low",
            "Medium",
            "High",
            "Very High",
            "Extremely High"});
            this.RocketLauncherComboBox.Location = new System.Drawing.Point(371, 67);
            this.RocketLauncherComboBox.Name = "RocketLauncherComboBox";
            this.RocketLauncherComboBox.Size = new System.Drawing.Size(103, 21);
            this.RocketLauncherComboBox.TabIndex = 86;
            // 
            // MachineGunComboBox
            // 
            this.MachineGunComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.MachineGunComboBox.FormattingEnabled = true;
            this.MachineGunComboBox.Items.AddRange(new object[] {
            "None",
            "Very Low",
            "Low",
            "Medium",
            "High",
            "Very High",
            "Extremely High"});
            this.MachineGunComboBox.Location = new System.Drawing.Point(371, 26);
            this.MachineGunComboBox.Name = "MachineGunComboBox";
            this.MachineGunComboBox.Size = new System.Drawing.Size(103, 21);
            this.MachineGunComboBox.TabIndex = 85;
            // 
            // PDWComboBox
            // 
            this.PDWComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.PDWComboBox.FormattingEnabled = true;
            this.PDWComboBox.Items.AddRange(new object[] {
            "None",
            "Very Low",
            "Low",
            "Medium",
            "High",
            "Very High",
            "Extremely High"});
            this.PDWComboBox.Location = new System.Drawing.Point(261, 67);
            this.PDWComboBox.Name = "PDWComboBox";
            this.PDWComboBox.Size = new System.Drawing.Size(90, 21);
            this.PDWComboBox.TabIndex = 84;
            // 
            // RifleComboBox
            // 
            this.RifleComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.RifleComboBox.FormattingEnabled = true;
            this.RifleComboBox.Items.AddRange(new object[] {
            "None",
            "Very Low",
            "Low",
            "Medium",
            "High",
            "Very High",
            "Extremely High"});
            this.RifleComboBox.Location = new System.Drawing.Point(261, 26);
            this.RifleComboBox.Name = "RifleComboBox";
            this.RifleComboBox.Size = new System.Drawing.Size(90, 21);
            this.RifleComboBox.TabIndex = 83;
            // 
            // ShotgunComboBox
            // 
            this.ShotgunComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ShotgunComboBox.FormattingEnabled = true;
            this.ShotgunComboBox.Items.AddRange(new object[] {
            "None",
            "Very Low",
            "Low",
            "Medium",
            "High",
            "Very High",
            "Extremely High"});
            this.ShotgunComboBox.Location = new System.Drawing.Point(146, 67);
            this.ShotgunComboBox.Name = "ShotgunComboBox";
            this.ShotgunComboBox.Size = new System.Drawing.Size(98, 21);
            this.ShotgunComboBox.TabIndex = 82;
            // 
            // HandgunComboBox
            // 
            this.HandgunComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.HandgunComboBox.FormattingEnabled = true;
            this.HandgunComboBox.Items.AddRange(new object[] {
            "None",
            "Very Low",
            "Low",
            "Medium",
            "High",
            "Very High",
            "Extremely High"});
            this.HandgunComboBox.Location = new System.Drawing.Point(146, 26);
            this.HandgunComboBox.Name = "HandgunComboBox";
            this.HandgunComboBox.Size = new System.Drawing.Size(98, 21);
            this.HandgunComboBox.TabIndex = 81;
            // 
            // RestrictionLevelComboBox
            // 
            this.RestrictionLevelComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.RestrictionLevelComboBox.FormattingEnabled = true;
            this.RestrictionLevelComboBox.Items.AddRange(new object[] {
            "None",
            "Licensed",
            "Restricted",
            "Military and Police",
            "Illegal"});
            this.RestrictionLevelComboBox.Location = new System.Drawing.Point(6, 67);
            this.RestrictionLevelComboBox.Name = "RestrictionLevelComboBox";
            this.RestrictionLevelComboBox.Size = new System.Drawing.Size(102, 21);
            this.RestrictionLevelComboBox.TabIndex = 80;
            // 
            // WealthLevelComboBox
            // 
            this.WealthLevelComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.WealthLevelComboBox.FormattingEnabled = true;
            this.WealthLevelComboBox.Items.AddRange(new object[] {
            "Very Low",
            "Low",
            "Medium",
            "High",
            "Very High"});
            this.WealthLevelComboBox.Location = new System.Drawing.Point(6, 26);
            this.WealthLevelComboBox.Name = "WealthLevelComboBox";
            this.WealthLevelComboBox.Size = new System.Drawing.Size(102, 21);
            this.WealthLevelComboBox.TabIndex = 79;
            this.WealthLevelComboBox.SelectedIndexChanged += new System.EventHandler(this.WealthLevelComboBox_SelectedIndexChanged);
            // 
            // aacheck
            // 
            this.aacheck.AutoSize = true;
            this.aacheck.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.aacheck.Location = new System.Drawing.Point(643, 62);
            this.aacheck.Name = "aacheck";
            this.aacheck.Size = new System.Drawing.Size(117, 18);
            this.aacheck.TabIndex = 78;
            this.aacheck.Text = "Advanced Arms";
            this.aacheck.UseVisualStyleBackColor = true;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Courier New", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(492, 9);
            this.label33.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(126, 14);
            this.label33.TabIndex = 77;
            this.label33.Text = "Grenade Launchers";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Courier New", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(492, 50);
            this.label30.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(77, 14);
            this.label30.TabIndex = 76;
            this.label30.Text = "Ammunition";
            // 
            // induscheck
            // 
            this.induscheck.AutoSize = true;
            this.induscheck.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.induscheck.Location = new System.Drawing.Point(643, 46);
            this.induscheck.Name = "induscheck";
            this.induscheck.Size = new System.Drawing.Size(96, 18);
            this.induscheck.TabIndex = 75;
            this.induscheck.Text = "Industrial";
            this.induscheck.UseVisualStyleBackColor = true;
            // 
            // lateinducheck
            // 
            this.lateinducheck.AutoSize = true;
            this.lateinducheck.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lateinducheck.Location = new System.Drawing.Point(643, 28);
            this.lateinducheck.Name = "lateinducheck";
            this.lateinducheck.Size = new System.Drawing.Size(131, 18);
            this.lateinducheck.TabIndex = 74;
            this.lateinducheck.Text = "Late Industrial";
            this.lateinducheck.UseVisualStyleBackColor = true;
            // 
            // earlymodcheck
            // 
            this.earlymodcheck.AutoSize = true;
            this.earlymodcheck.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.earlymodcheck.Location = new System.Drawing.Point(643, 9);
            this.earlymodcheck.Name = "earlymodcheck";
            this.earlymodcheck.Size = new System.Drawing.Size(110, 18);
            this.earlymodcheck.TabIndex = 73;
            this.earlymodcheck.Text = "Early Modern";
            this.earlymodcheck.UseVisualStyleBackColor = true;
            // 
            // GrenadeCheckbox
            // 
            this.GrenadeCheckbox.AutoSize = true;
            this.GrenadeCheckbox.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GrenadeCheckbox.Location = new System.Drawing.Point(771, 9);
            this.GrenadeCheckbox.Margin = new System.Windows.Forms.Padding(2);
            this.GrenadeCheckbox.Name = "GrenadeCheckbox";
            this.GrenadeCheckbox.Size = new System.Drawing.Size(82, 18);
            this.GrenadeCheckbox.TabIndex = 72;
            this.GrenadeCheckbox.Text = "Grenades";
            this.GrenadeCheckbox.UseVisualStyleBackColor = true;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Courier New", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(368, 50);
            this.label28.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(119, 14);
            this.label28.TabIndex = 71;
            this.label28.Text = "Rocket Launchers";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Courier New", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(368, 9);
            this.label25.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(91, 14);
            this.label25.TabIndex = 70;
            this.label25.Text = "Machine Guns";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Courier New", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(258, 50);
            this.label22.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(35, 14);
            this.label22.TabIndex = 69;
            this.label22.Text = "PDWs";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Courier New", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(258, 9);
            this.label19.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(49, 14);
            this.label19.TabIndex = 68;
            this.label19.Text = "Rifles";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Courier New", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(143, 50);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(63, 14);
            this.label14.TabIndex = 67;
            this.label14.Text = "Shotguns";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Courier New", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(3, 50);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(126, 14);
            this.label6.TabIndex = 66;
            this.label6.Text = "Restriction Level";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Courier New", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(143, 9);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 14);
            this.label5.TabIndex = 65;
            this.label5.Text = "Handguns";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Courier New", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 9);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 14);
            this.label1.TabIndex = 64;
            this.label1.Text = "Wealth Level";
            // 
            // tabPage2
            // 
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1124, 128);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "tabPage2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Courier New", 7.8F);
            this.button2.Location = new System.Drawing.Point(1150, 415);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(108, 132);
            this.button2.TabIndex = 64;
            this.button2.Text = "Generate";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // Gunstoreform
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1263, 559);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.storetabs);
            this.Controls.Add(this.gunstoreoutput);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "Gunstoreform";
            this.Text = "Weapon Store Generator";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            this.storetabs.ResumeLayout(false);
            this.rangedtab.ResumeLayout(false);
            this.rangedtab.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox gunstoreoutput;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.BindingSource bindingSource1;
        private System.Windows.Forms.TabPage rangedtab;
        private System.Windows.Forms.ComboBox AmmunitionComboBox;
        private System.Windows.Forms.ComboBox GrenadeLauncherComboBox;
        private System.Windows.Forms.ComboBox RocketLauncherComboBox;
        private System.Windows.Forms.ComboBox MachineGunComboBox;
        private System.Windows.Forms.ComboBox PDWComboBox;
        private System.Windows.Forms.ComboBox RifleComboBox;
        private System.Windows.Forms.ComboBox ShotgunComboBox;
        private System.Windows.Forms.ComboBox HandgunComboBox;
        private System.Windows.Forms.ComboBox RestrictionLevelComboBox;
        private System.Windows.Forms.ComboBox WealthLevelComboBox;
        private System.Windows.Forms.CheckBox aacheck;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.CheckBox induscheck;
        private System.Windows.Forms.CheckBox lateinducheck;
        private System.Windows.Forms.CheckBox earlymodcheck;
        private System.Windows.Forms.CheckBox GrenadeCheckbox;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPage2;
        public System.Windows.Forms.TabControl storetabs;
        private System.Windows.Forms.Button button2;
    }
}