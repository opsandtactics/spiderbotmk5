﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spiderbotmk5
{
    public class Mre
    {
        public string Container { get; set; }
        public List<string> Entree { get ; set; }
        public List<string> Bread { get; set; }
        public List<string> Spread { get; set; }
        public string Dessert { get; set; }
        public string DrinkContainer { get; set; }
        public string Drink { get; set; }
        public List<string> Extras { get; set; }

        public Mre()
            {
            this.Entree = new List<string>();
            this.Bread = new List<string>();
            this.Spread = new List<string>();
            this.Extras = new List<string>();
            }
    }

}
