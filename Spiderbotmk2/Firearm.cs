﻿using System;
using System.IO;
using System.Web.Script.Serialization;

public class Firearm
{
    public string Name { get; set; }
    public string Caliber { get; set; }
    public string Magazine { get; set; }
    public string Upgrade { get; set; }
    public string Range { get; set; }
    public string RateofFire { get; set; }
    public string Size { get; set; }
    public int Weight { get; set; }
    public double Cost { get; set; }
    public double Error { get; set; }
    public string License { get; set; }
    public string Notes { get; set; }

    public Firearm (string csvLine)
	{

        using (Microsoft.VisualBasic.FileIO.TextFieldParser parser = new Microsoft.VisualBasic.FileIO.TextFieldParser(new StringReader(csvLine)))
        {
            parser.TextFieldType = Microsoft.VisualBasic.FileIO.FieldType.Delimited;
            parser.HasFieldsEnclosedInQuotes = true;
            parser.Delimiters = new string[] { "," };

            string[] values;
            while (!parser.EndOfData)
            {
                values = parser.ReadFields();
                Name = values[0];
                Caliber = values[1];
                Magazine = values[2];
                Upgrade = values[3];
                Range = values[4];
                RateofFire = values[5];
                Size = values[6];
                Weight = Convert.ToInt32(values[7]);
                Cost = Convert.ToDouble(values[8]);
                Error = Convert.ToDouble(values[9]);
                License = values[10];
                Notes = values[11];


            }
        }


    }

    public Firearm()
    {
    }

    public override string ToString()
    {
        System.Text.StringBuilder conversion = new System.Text.StringBuilder();
        conversion.Append(Name + " | Caliber: " + Caliber + " | Magazine:" + Magazine + " | Upgrade Points:" + Upgrade + " | Range:" + Range + " | ROF:" + RateofFire + " | Size:" + Size + " | Weight:" + Weight.ToString() + "oz" + " | Cost:" + Cost.ToString() + " WP" + " | Error Range:" + Error.ToString() + "%" + " | License:" + License + Environment.NewLine + "Standard Equipment:" + Notes);
        return conversion.ToString();
    }


}
