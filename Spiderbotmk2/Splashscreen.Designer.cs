﻿namespace spiderbotmk5
{
    partial class Splashscreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Splashscreen));
            this.mrebutton = new System.Windows.Forms.Button();
            this.gunstorebutton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // mrebutton
            // 
            this.mrebutton.Location = new System.Drawing.Point(12, 99);
            this.mrebutton.Name = "mrebutton";
            this.mrebutton.Size = new System.Drawing.Size(87, 68);
            this.mrebutton.TabIndex = 0;
            this.mrebutton.Text = "MRE Generator";
            this.mrebutton.UseVisualStyleBackColor = true;
            this.mrebutton.Click += new System.EventHandler(this.button1_Click);
            // 
            // gunstorebutton
            // 
            this.gunstorebutton.Location = new System.Drawing.Point(105, 99);
            this.gunstorebutton.Name = "gunstorebutton";
            this.gunstorebutton.Size = new System.Drawing.Size(87, 68);
            this.gunstorebutton.TabIndex = 1;
            this.gunstorebutton.Text = "Gunstore Generator";
            this.gunstorebutton.UseVisualStyleBackColor = true;
            this.gunstorebutton.Click += new System.EventHandler(this.button2_Click);
            // 
            // Splashscreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(206, 179);
            this.Controls.Add(this.gunstorebutton);
            this.Controls.Add(this.mrebutton);
            this.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Splashscreen";
            this.Text = "Spiderbot Mk5";
            this.Load += new System.EventHandler(this.Splashscreen_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button mrebutton;
        private System.Windows.Forms.Button gunstorebutton;
    }
}