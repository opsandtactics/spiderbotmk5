﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Web.Script.Serialization;
using System.Data.SQLite;

namespace spiderbotmk5
{
public partial class Gunstoreform : Form
{
    public Gunstoreform()
    {
        InitializeComponent();
    }

    private void Form1_Load(object sender, EventArgs e)
    {
                }

    public void button2_Click(object sender, EventArgs e)
    {
            
    }

    private void label2_Click(object sender, EventArgs e)
    {

    }

    private void label3_Click(object sender, EventArgs e)
    {

    }

    private void label5_Click(object sender, EventArgs e)
    {

    }

    private void label10_Click(object sender, EventArgs e)
    {

    }

    private void label28_Click(object sender, EventArgs e)
    {

    }

    private void label27_Click(object sender, EventArgs e)
    {

    }

    private void trackBar7_Scroll(object sender, EventArgs e)
    {

    }

    private void label26_Click(object sender, EventArgs e)
    {

    }

    private void label25_Click(object sender, EventArgs e)
    {

    }

    private void trackBar3_Scroll(object sender, EventArgs e)
    {

    }

    private void saveToolStripMenuItem_Click(object sender, EventArgs e)
    {
        SaveFileDialog SaveStoreDialog = new SaveFileDialog();
        SaveStoreDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
        SaveStoreDialog.FileName = "NewStore.txt";
        SaveStoreDialog.RestoreDirectory = true;

        if (SaveStoreDialog.ShowDialog() == DialogResult.OK)
        {
            using (StreamWriter newstream = new StreamWriter(SaveStoreDialog.FileName))
            {

                newstream.Write(gunstoreoutput.Text);
                newstream.Close();
            }
        }
    }

    private void exitToolStripMenuItem_Click(object sender, EventArgs e)
    {
        if (MessageBox.Show("Are you sure you want to quit, Sempai?", "Exit", MessageBoxButtons.OKCancel) == DialogResult.OK)
        {

            Application.Exit();

        }
    }

    private void label30_Click(object sender, EventArgs e)
    {

    }

    private void label29_Click(object sender, EventArgs e)
    {

    }

    private void trackBar1_Scroll(object sender, EventArgs e)
    {

    }

    private void label7_Click(object sender, EventArgs e)
    {

    }

    private void label33_Click(object sender, EventArgs e)
    {

    }

    private void label32_Click(object sender, EventArgs e)
    {

    }

    private void label31_Click(object sender, EventArgs e)
    {

    }

    private void trackBar1_Scroll_1(object sender, EventArgs e)
    {

    }

    private void button1_Click(object sender, EventArgs e)
    {

    }

    private void checkBox1_CheckedChanged(object sender, EventArgs e)
    {

    }

    private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    private void comboBox4_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    private void tabPage1_Click(object sender, EventArgs e)
    {

    }

    private void WealthLevelComboBox_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    private void comboBox1_SelectedIndexChanged_1(object sender, EventArgs e)
    {

    }

        private void button2_Click_1(object sender, EventArgs e)
        {



            string handgunlabel = "";
            string shotgunlabel = "";
            string riflelabel = "";
            string pdwlabel = "";
            string mglabel = "";
            string ammolabel = "";
            string gllabel = "";
            string rocketlabel = "";
            string handgunstring = "";
            string shotgunstring = "";
            string riflestring = "";
            string pdwstring = "";
            string mgstring = "";
            string ammostring = "";
            string glstring = "";
            string rocketstring = "";
            object OverAmmoMan = "";
            object OverAmmoLoad = "";
            object OverBoxKey = "";
            string OverAmmotype = "";
            object OverCategoryType = "";
            int OverAmmoCost = 0;
            object OverPerBoxAmmount = "";
            object OverRestriction = "";
            object OverLoadingID = "";
            string OverLoadingName = "";
            object OverLoadingDesc = "";
            Random handgunroller = new Random();
            Random PDWroller = new Random();
            Random rifleroller = new Random();
            Random shotgunroller = new Random();
            Random mgroller = new Random();
            Random rocketroller = new Random();
            Random glroller = new Random();
            Random gunroller = new Random();
            Random ammoroller = new Random();
            // this takes the restriction level and allows or denies certain kinds of guns

            string r = RestrictionLevelComboBox.Text;
            string w = WealthLevelComboBox.Text;
            string hand = HandgunComboBox.Text;
            string shot = ShotgunComboBox.Text;
            string rifle = RifleComboBox.Text;
            string pdw = PDWComboBox.Text;
            string mg = MachineGunComboBox.Text;
            string ammo = AmmunitionComboBox.Text;
            string gnadelaunc = GrenadeLauncherComboBox.Text;
            string rocketlaunch = RocketLauncherComboBox.Text;
            int handcount = 0;
            int shotcount = 0;
            int riflecount = 0;
            int PDWcount = 0;
            int mgcount = 0;
            int ammocount = 0;
            int amountcount = 1;
            int glcount = 0;
            int rocketcount = 0;
            string SQLRestrictionInput = "N";
            string SQLWealthInput = "Cheap";

            switch (r)// This one checks and modifies the sql entry based on what restriction setting is set
            {
                case "None":
                    SQLRestrictionInput = "N";
                    break;
                case "Licensed":
                    SQLRestrictionInput = "N%' OR License LIKE '%L";
                    break;
                case "Restricted":
                    SQLRestrictionInput = "N%' OR License LIKE '%L%' OR License LIKE '%R";
                    break;
                case "Military and Police":
                    SQLRestrictionInput = "N%' OR License LIKE '%L%' OR License LIKE '%R%' OR License LIKE '%M&P";
                    break;
                case "Illegal":
                    SQLRestrictionInput = "N%' OR License LIKE '%L%' OR License LIKE '%R%' OR License LIKE '%M&P%' OR License LIKE '%I";
                    break;
                default:
                    SQLRestrictionInput = "N";
                    break;
            }
            switch (w)// This one checks and modifies the sql entry based on what wealth setting is set. Five settings that modify the possible guns one can get
            {
                case "Very Low":
                    SQLWealthInput = "Cheap";
                    break;
                case "Low":
                    SQLWealthInput = "Cheap%' OR Category LIKE '%Mid-Range";
                    break;
                case "Medium":
                    SQLWealthInput = "Mid-Range";
                    break;
                case "High":
                    SQLWealthInput = "Mid-Range%' OR Category LIKE '%Expensive";
                    break;
                case "Very High":
                    SQLWealthInput = "Expensive";
                    break;
                default:
                    SQLWealthInput = "Cheap";
                    break;
            }

            gunstoreoutput.Text = "";
            // We're replacing all the goddamn JSons with a database!
            // Hooray :D
            SQLiteConnection sqlite_conn;
            SQLiteCommand sqlite_cmd;
            SQLiteDataReader sqlite_datareader;

            sqlite_conn = new SQLiteConnection("Data Source=C://Users//Alex//Dropbox//Guns;Version = 3;");// Location of the DB at home
                                                                                                          //sqlite_conn = new SQLiteConnection("Data Source=C://Users//amazyck//Dropbox//Guns;Version = 3;");// Location of the DB at work
            sqlite_conn.Open();


            switch (hand)// This one with this big chunk generates handguns
            {
                case "None":
                    handcount = 0;
                    break;
                case "Very Low":
                    handcount = handgunroller.Next(1, 3);
                    break;
                case "Low":
                    handcount = handgunroller.Next(2, 5);
                    break;
                case "Medium":
                    handcount = handgunroller.Next(5, 11);
                    break;
                case "High":
                    handcount = handgunroller.Next(8, 16);
                    break;
                case "Very High":
                    handcount = handgunroller.Next(12, 21);
                    break;
                case "Extremely High":
                    handcount = handgunroller.Next(16, 31);
                    break;
                default:
                    handcount = 0;
                    break;
            }
            sqlite_cmd = sqlite_conn.CreateCommand();

            //sqlite_cmd.CommandText = "SELECT * FROM FirearmTable WHERE License LIKE '%" + SQLRestrictionInput + "%' and Category LIKE '%" + SQLWealthInput + "%' ORDER BY RANDOM() LIMIT " + handcount.ToString();
            sqlite_cmd.CommandText = "SELECT * FROM FirearmTable WHERE (Category LIKE '%Modern Holdout Handgun' OR Category LIKE '%Modern Backup Handgun' OR Category LIKE '%Modern Full Size Handgun' OR Category LIKE '%Modern Target AND Hunting Handgun') AND (License LIKE '%" + SQLRestrictionInput + "%') AND (Category LIKE '%" + SQLWealthInput + "%') AND NOT (Category LIKE 'Early%') ORDER BY RANDOM() LIMIT " + handcount.ToString();
            sqlite_datareader = sqlite_cmd.ExecuteReader();
            if (handcount != 0)
            {
                handgunlabel = "__________________________Handguns__________________________" + Environment.NewLine + Environment.NewLine;
            }
            else
            {
                handgunlabel = "";
            }
            handgunstring += handgunlabel;
            while (sqlite_datareader.Read())
            {
                // Print out the content of the text field:
                // System.Console.WriteLine("DEBUG Output: '" + sqlite_datareader["text"] + "'");
                //0 Weapon Name 1 Caliber 2 Magazine 3 Upgrade Slot 4 Range 5 ROF 6 Size 7 Weight 8Cost 9error range 10 license 11 Notes 12 categories(Don't show this one)
                object WeaponName = sqlite_datareader.GetValue(0);
                string Caliber = sqlite_datareader.GetString(1);
                string Magazine = sqlite_datareader.GetString(2);
                string UpgradeSlot = sqlite_datareader.GetString(3);
                string Range = sqlite_datareader.GetString(4);
                string ROF = sqlite_datareader.GetString(5);
                string size = sqlite_datareader.GetString(6);
                double Weight = sqlite_datareader.GetDouble(7);
                int Cost = sqlite_datareader.GetInt32(8);
                int Error = sqlite_datareader.GetInt32(9);
                string License = sqlite_datareader.GetString(10);
                string Notes = sqlite_datareader.GetString(11);
                switch (hand)
                {
                    case "None":
                        amountcount = 0;
                        break;
                    case "Very Low":
                        amountcount = gunroller.Next(1, 3);
                        break;
                    case "Low":
                        amountcount = gunroller.Next(2, 3);
                        break;
                    case "Medium":
                        amountcount = gunroller.Next(2, 4);
                        break;
                    case "High":
                        amountcount = gunroller.Next(2, 4);
                        break;
                    case "Very High":
                        amountcount = gunroller.Next(2, 5);
                        break;
                    case "Extremely High":
                        amountcount = gunroller.Next(2, 5);
                        break;
                    default:
                        amountcount = 1;
                        break;
                }


                handgunstring += amountcount.ToString() + "x " + WeaponName + " | Caliber: " + Caliber + " | Magazine: " + Magazine + " | Upgrade Points: " + UpgradeSlot + " | Range: " + Range + " | ROF:" + ROF + " | Weight: " + Weight.ToString() + " | Cost: " + Cost.ToString() + " | Error Range: " + Error.ToString() + "% | License: " + License + " | Standard Equipment: " + Notes + " " + Environment.NewLine + Environment.NewLine;

            }


            switch (shot)// This one with this big chunk generates Shotguns
            {
                case "None":
                    shotcount = 0;
                    break;
                case "Very Low":
                    shotcount = shotgunroller.Next(1, 3);
                    break;
                case "Low":
                    shotcount = shotgunroller.Next(2, 5);
                    break;
                case "Medium":
                    shotcount = shotgunroller.Next(5, 11);
                    break;
                case "High":
                    shotcount = shotgunroller.Next(8, 16);
                    break;
                case "Very High":
                    shotcount = shotgunroller.Next(12, 21);
                    break;
                case "Extremely High":
                    shotcount = shotgunroller.Next(16, 31);
                    break;
                default:
                    shotcount = 0;
                    break;
            }
            sqlite_cmd = sqlite_conn.CreateCommand();
            sqlite_cmd.CommandText = "SELECT * FROM FirearmTable WHERE (Category LIKE '%Modern Sporting Shotgun' OR Category LIKE '%Modern Combat Shotgun') AND (License LIKE '%" + SQLRestrictionInput + "%') AND (Category LIKE '%" + SQLWealthInput + "%') ORDER BY RANDOM() LIMIT " + shotcount.ToString();
            sqlite_datareader = sqlite_cmd.ExecuteReader();




            if (shotcount != 0)
            {
                shotgunlabel = "__________________________Shotguns__________________________" + Environment.NewLine + Environment.NewLine;
            }
            else
            {
                shotgunlabel = "";
            }
            shotgunstring += shotgunlabel;
            while (sqlite_datareader.Read())
            {
                // Print out the content of the text field:
                // System.Console.WriteLine("DEBUG Output: '" + sqlite_datareader["text"] + "'");
                //0 Weapon Name 1 Caliber 2 Magazine 3 Upgrade Slot 4 Range 5 ROF 6 Size 7 Weight 8Cost 9error range 10 license 11 Notes 12 categories(Don't show this one)
                object WeaponName = sqlite_datareader.GetValue(0);
                string Caliber = sqlite_datareader.GetString(1);
                string Magazine = sqlite_datareader.GetString(2);
                string UpgradeSlot = sqlite_datareader.GetString(3);
                string Range = sqlite_datareader.GetString(4);
                string ROF = sqlite_datareader.GetString(5);
                string size = sqlite_datareader.GetString(6);
                double Weight = sqlite_datareader.GetDouble(7);
                int Cost = sqlite_datareader.GetInt32(8);
                int Error = sqlite_datareader.GetInt32(9);
                string License = sqlite_datareader.GetString(10);
                string Notes = sqlite_datareader.GetString(11);

                switch (shot)
                {
                    case "None":
                        amountcount = 0;
                        break;
                    case "Very Low":
                        amountcount = gunroller.Next(1, 3);
                        break;
                    case "Low":
                        amountcount = gunroller.Next(2, 3);
                        break;
                    case "Medium":
                        amountcount = gunroller.Next(2, 4);
                        break;
                    case "High":
                        amountcount = gunroller.Next(2, 4);
                        break;
                    case "Very High":
                        amountcount = gunroller.Next(2, 5);
                        break;
                    case "Extremely High":
                        amountcount = gunroller.Next(2, 5);
                        break;
                    default:
                        amountcount = 1;
                        break;
                }

                shotgunstring += amountcount.ToString() + "x " + WeaponName + " | Caliber: " + Caliber + " | Magazine: " + Magazine + " | Upgrade Points: " + UpgradeSlot + " | Range: " + Range + " | ROF:" + ROF + " | Weight: " + Weight.ToString() + " | Cost: " + Cost.ToString() + " | Error Range: " + Error.ToString() + "% | License: " + License + " | Standard Equipment: " + Notes + " " + Environment.NewLine + Environment.NewLine;

            }

            switch (rifle)// This one with this big chunk generates Rifles
            {
                case "None":
                    riflecount = 0;
                    break;
                case "Very Low":
                    riflecount = rifleroller.Next(1, 3);
                    break;
                case "Low":
                    riflecount = rifleroller.Next(2, 5);
                    break;
                case "Medium":
                    riflecount = rifleroller.Next(5, 11);
                    break;
                case "High":
                    riflecount = rifleroller.Next(8, 16);
                    break;
                case "Very High":
                    riflecount = rifleroller.Next(12, 21);
                    break;
                case "Extremely High":
                    riflecount = rifleroller.Next(16, 31);
                    break;
                default:
                    riflecount = 0;
                    break;
            }
            sqlite_cmd = sqlite_conn.CreateCommand();
            sqlite_cmd.CommandText = "SELECT * FROM FirearmTable WHERE (Category LIKE '%Modern Carbine' OR Category LIKE '%Modern Assault Rifle' OR Category LIKE '%Modern Bolt Action Rifle' OR Category LIKE '%Modern Anti-Material Rifle') AND (License LIKE '%" + SQLRestrictionInput + "%') AND (Category LIKE '%" + SQLWealthInput + "%') AND NOT (Category LIKE 'Early%') ORDER BY RANDOM() LIMIT " + riflecount.ToString();
            sqlite_datareader = sqlite_cmd.ExecuteReader();




            if (riflecount != 0)
            {
                riflelabel = "__________________________Rifles__________________________" + Environment.NewLine + Environment.NewLine;
            }
            else
            {
                riflelabel = "";
            }
            riflestring += riflelabel;
            while (sqlite_datareader.Read())
            {
                // Print out the content of the text field:
                // System.Console.WriteLine("DEBUG Output: '" + sqlite_datareader["text"] + "'");
                //0 Weapon Name 1 Caliber 2 Magazine 3 Upgrade Slot 4 Range 5 ROF 6 Size 7 Weight 8Cost 9error range 10 license 11 Notes 12 categories(Don't show this one)
                object WeaponName = sqlite_datareader.GetValue(0);
                string Caliber = sqlite_datareader.GetString(1);
                string Magazine = sqlite_datareader.GetString(2);
                string UpgradeSlot = sqlite_datareader.GetString(3);
                string Range = sqlite_datareader.GetString(4);
                string ROF = sqlite_datareader.GetString(5);
                string size = sqlite_datareader.GetString(6);
                double Weight = sqlite_datareader.GetDouble(7);
                int Cost = sqlite_datareader.GetInt32(8);
                int Error = sqlite_datareader.GetInt32(9);
                string License = sqlite_datareader.GetString(10);
                string Notes = sqlite_datareader.GetString(11);

                switch (rifle)
                {
                    case "None":
                        amountcount = 0;
                        break;
                    case "Very Low":
                        amountcount = gunroller.Next(1, 3);
                        break;
                    case "Low":
                        amountcount = gunroller.Next(2, 3);
                        break;
                    case "Medium":
                        amountcount = gunroller.Next(2, 4);
                        break;
                    case "High":
                        amountcount = gunroller.Next(2, 4);
                        break;
                    case "Very High":
                        amountcount = gunroller.Next(2, 5);
                        break;
                    case "Extremely High":
                        amountcount = gunroller.Next(2, 5);
                        break;
                    default:
                        amountcount = 1;
                        break;
                }
                riflestring += amountcount.ToString() + "x " + WeaponName + " | Caliber: " + Caliber + " | Magazine: " + Magazine + " | Upgrade Points: " + UpgradeSlot + " | Range: " + Range + " | ROF:" + ROF + " | Weight: " + Weight.ToString() + " | Cost: " + Cost.ToString() + " | Error Range: " + Error.ToString() + "% | License: " + License + " | Standard Equipment: " + Notes + " " + Environment.NewLine + Environment.NewLine;

            }


            switch (pdw)// This one with this big chunk generates PDWs
            {
                case "None":
                    PDWcount = 0;
                    break;
                case "Very Low":
                    PDWcount = PDWroller.Next(1, 3);
                    break;
                case "Low":
                    PDWcount = PDWroller.Next(2, 5);
                    break;
                case "Medium":
                    PDWcount = PDWroller.Next(5, 11);
                    break;
                case "High":
                    PDWcount = PDWroller.Next(8, 16);
                    break;
                case "Very High":
                    PDWcount = PDWroller.Next(12, 21);
                    break;
                case "Extremely High":
                    PDWcount = PDWroller.Next(16, 31);
                    break;
                default:
                    PDWcount = 0;
                    break;
            }
            sqlite_cmd = sqlite_conn.CreateCommand();
            sqlite_cmd.CommandText = "SELECT * FROM FirearmTable WHERE (Category LIKE '%Modern Machine Pistol' OR Category LIKE '%Modern SMG') AND (License LIKE '%" + SQLRestrictionInput + "%') AND (Category LIKE '%" + SQLWealthInput + "%') AND NOT (Category LIKE 'Early%') ORDER BY RANDOM() LIMIT " + PDWcount.ToString();
            sqlite_datareader = sqlite_cmd.ExecuteReader();

            if (PDWcount != 0)
            {
                pdwlabel = "__________________________PDWs__________________________" + Environment.NewLine + Environment.NewLine;
            }
            else
            {
                pdwlabel = "";
            }
            pdwstring += pdwlabel;
            while (sqlite_datareader.Read())
            {
                // Print out the content of the text field:
                // System.Console.WriteLine("DEBUG Output: '" + sqlite_datareader["text"] + "'");
                //0 Weapon Name 1 Caliber 2 Magazine 3 Upgrade Slot 4 Range 5 ROF 6 Size 7 Weight 8Cost 9error range 10 license 11 Notes 12 categories(Don't show this one)
                object WeaponName = sqlite_datareader.GetValue(0);
                string Caliber = sqlite_datareader.GetString(1);
                string Magazine = sqlite_datareader.GetString(2);
                string UpgradeSlot = sqlite_datareader.GetString(3);
                string Range = sqlite_datareader.GetString(4);
                string ROF = sqlite_datareader.GetString(5);
                string size = sqlite_datareader.GetString(6);
                double Weight = sqlite_datareader.GetDouble(7);
                int Cost = sqlite_datareader.GetInt32(8);
                int Error = sqlite_datareader.GetInt32(9);
                string License = sqlite_datareader.GetString(10);
                string Notes = sqlite_datareader.GetString(11);

                switch (pdw)
                {
                    case "None":
                        amountcount = 0;
                        break;
                    case "Very Low":
                        amountcount = gunroller.Next(1, 3);
                        break;
                    case "Low":
                        amountcount = gunroller.Next(2, 3);
                        break;
                    case "Medium":
                        amountcount = gunroller.Next(2, 4);
                        break;
                    case "High":
                        amountcount = gunroller.Next(2, 4);
                        break;
                    case "Very High":
                        amountcount = gunroller.Next(2, 5);
                        break;
                    case "Extremely High":
                        amountcount = gunroller.Next(2, 5);
                        break;
                    default:
                        amountcount = 1;
                        break;
                }
                pdwstring += amountcount.ToString() + "x " + WeaponName + " | Caliber: " + Caliber + " | Magazine: " + Magazine + " | Upgrade Points: " + UpgradeSlot + " | Range: " + Range + " | ROF:" + ROF + " | Weight: " + Weight.ToString() + " | Cost: " + Cost.ToString() + " | Error Range: " + Error.ToString() + "% | License: " + License + " | Standard Equipment: " + Notes + " " + Environment.NewLine + Environment.NewLine;

            }

            switch (mg)// This one with this big chunk generates MG! Ho Ho ho!
            {
                case "None":
                    mgcount = 0;
                    break;
                case "Very Low":
                    mgcount = mgroller.Next(1, 3);
                    break;
                case "Low":
                    mgcount = mgroller.Next(2, 5);
                    break;
                case "Medium":
                    mgcount = mgroller.Next(5, 11);
                    break;
                case "High":
                    mgcount = mgroller.Next(8, 16);
                    break;
                case "Very High":
                    mgcount = mgroller.Next(12, 21);
                    break;
                case "Extremely High":
                    mgcount = mgroller.Next(16, 31);
                    break;
                default:
                    mgcount = 0;
                    break;
            }
            sqlite_cmd = sqlite_conn.CreateCommand();
            sqlite_cmd.CommandText = "SELECT * FROM FirearmTable WHERE (Category LIKE '%Modern Light Machine Gun' OR Category LIKE '%Modern General Purpose Machine Gun') AND (License LIKE '%" + SQLRestrictionInput + "%') AND NOT (Category LIKE 'Early%') ORDER BY RANDOM() LIMIT " + mgcount.ToString();
            sqlite_datareader = sqlite_cmd.ExecuteReader();

            if (mgcount != 0)
            {
                mglabel = "__________________________Machine Guns__________________________" + Environment.NewLine + Environment.NewLine;
            }
            else
            {
                mglabel = "";
            }
            mgstring += mglabel;
            while (sqlite_datareader.Read())
            {
                // Print out the content of the text field:
                // System.Console.WriteLine("DEBUG Output: '" + sqlite_datareader["text"] + "'");
                //0 Weapon Name 1 Caliber 2 Magazine 3 Upgrade Slot 4 Range 5 ROF 6 Size 7 Weight 8Cost 9error range 10 license 11 Notes 12 categories(Don't show this one)
                object WeaponName = sqlite_datareader.GetValue(0);
                string Caliber = sqlite_datareader.GetString(1);
                string Magazine = sqlite_datareader.GetString(2);
                string UpgradeSlot = sqlite_datareader.GetString(3);
                string Range = sqlite_datareader.GetString(4);
                string ROF = sqlite_datareader.GetString(5);
                string size = sqlite_datareader.GetString(6);
                double Weight = sqlite_datareader.GetDouble(7);
                int Cost = sqlite_datareader.GetInt32(8);
                int Error = sqlite_datareader.GetInt32(9);
                string License = sqlite_datareader.GetString(10);
                string Notes = sqlite_datareader.GetString(11);
                switch (mg)
                {
                    case "None":
                        amountcount = 0;
                        break;
                    case "Very Low":
                        amountcount = gunroller.Next(1, 3);
                        break;
                    case "Low":
                        amountcount = gunroller.Next(2, 3);
                        break;
                    case "Medium":
                        amountcount = gunroller.Next(2, 4);
                        break;
                    case "High":
                        amountcount = gunroller.Next(2, 4);
                        break;
                    case "Very High":
                        amountcount = gunroller.Next(2, 5);
                        break;
                    case "Extremely High":
                        amountcount = gunroller.Next(2, 5);
                        break;
                    default:
                        amountcount = 1;
                        break;
                }

                mgstring += amountcount.ToString() + "x " + WeaponName + " | Caliber: " + Caliber + " | Magazine: " + Magazine + " | Upgrade Points: " + UpgradeSlot + " | Range: " + Range + " | ROF:" + ROF + " | Weight: " + Weight.ToString() + " | Cost: " + Cost.ToString() + " | Error Range: " + Error.ToString() + "% | License: " + License + " | Standard Equipment: " + Notes + " " + Environment.NewLine + Environment.NewLine;

            }




            switch (ammo)// This one with this big chunk generates ammo
            {
                case "None":
                    ammocount = 0;
                    break;
                case "Very Low":
                    ammocount = ammoroller.Next(3, 6);
                    break;
                case "Low":
                    ammocount = ammoroller.Next(5, 10);
                    break;
                case "Medium":
                    ammocount = ammoroller.Next(9, 18);
                    break;
                case "High":
                    ammocount = ammoroller.Next(11, 22);
                    break;
                case "Very High":
                    ammocount = ammoroller.Next(15, 30);
                    break;
                case "Extremely High":
                    ammocount = ammoroller.Next(18, 36);
                    break;
                default:
                    ammocount = 0;
                    break;
            }





            if (ammocount != 0)
            {
                ammolabel = "__________________________Firearm Ammunition__________________________" + Environment.NewLine + Environment.NewLine;
            }
            else
            {
                ammolabel = "";
            }

            ammostring += ammolabel;
            while (ammocount != 0)
            {
                sqlite_cmd = sqlite_conn.CreateCommand();// Gets the Ammomaker's name
                sqlite_cmd.CommandText = "SELECT * FROM AmmoMakers ORDER BY RANDOM() LIMIT " + ammocount.ToString();
                sqlite_datareader = sqlite_cmd.ExecuteReader();
                while (sqlite_datareader.Read())
                {
                    //0 Ammoname
                    object AmmoMan = sqlite_datareader.GetValue(0);
                    OverAmmoMan = AmmoMan;
                }

                sqlite_cmd = sqlite_conn.CreateCommand();// Gets the Possible ammo boxes Type
                sqlite_cmd.CommandText = "SELECT * FROM AmmoBoxes WHERE License LIKE '%" + SQLRestrictionInput + "%' AND NOT (Category LIKE 'Advanced Arms%') ORDER BY RANDOM() LIMIT " + ammocount.ToString();
                sqlite_datareader = sqlite_cmd.ExecuteReader();

                while (sqlite_datareader.Read())
                {
                    //0 The stuff is obvious, look at the object names
                    object BoxKey = sqlite_datareader.GetValue(0);
                    string Ammotype = sqlite_datareader.GetString(1);
                    object CategoryType = sqlite_datareader.GetValue(2);
                    int AmmoCost = sqlite_datareader.GetInt32(3);
                    object PerBoxAmmount = sqlite_datareader.GetValue(4);
                    object Restriction = sqlite_datareader.GetValue(5);
                    object Category = sqlite_datareader.GetValue(6);

                    OverBoxKey = BoxKey;
                    OverAmmotype = Ammotype;
                    OverCategoryType = CategoryType;
                    OverAmmoCost = AmmoCost;
                    OverPerBoxAmmount = PerBoxAmmount;
                    OverRestriction = Restriction;

                }

                sqlite_cmd = sqlite_conn.CreateCommand();// Gets the Possible ammo boxes Type
                sqlite_cmd.CommandText = "SELECT * FROM AmmoLoading ORDER BY RANDOM() LIMIT " + ammocount.ToString();
                sqlite_datareader = sqlite_cmd.ExecuteReader();

                while (sqlite_datareader.Read())
                {
                    //0 The stuff is obvious, look at the object names
                    object LoadingID = sqlite_datareader.GetValue(0);
                    string LoadingName = sqlite_datareader.GetString(1);
                    object LoadingDesc = sqlite_datareader.GetValue(2);


                    OverLoadingID = LoadingID;
                    OverLoadingName = LoadingName;
                    OverLoadingDesc = LoadingDesc;

                    switch (ammo)
                    {
                        case "None":
                            amountcount = 0;
                            break;
                        case "Very Low":
                            amountcount = gunroller.Next(4, 6);
                            break;
                        case "Low":
                            amountcount = gunroller.Next(5, 10);
                            break;
                        case "Medium":
                            amountcount = gunroller.Next(8, 12);
                            break;
                        case "High":
                            amountcount = gunroller.Next(10, 15);
                            break;
                        case "Very High":
                            amountcount = gunroller.Next(15, 25);
                            break;
                        case "Extremely High":
                            amountcount = gunroller.Next(20, 30);
                            break;
                        default:
                            amountcount = 1;
                            break;
                    }

                }

                if (amountcount == 0)
                {
                    ammostring = "";
                }
                else if (amountcount == 1)
                {
                    if (OverAmmotype.Contains("Shotshell"))
                    {
                        ammostring += OverAmmotype + " | " + amountcount + " Box of " + OverCategoryType + ", " + OverAmmoMan + " Brand. " + OverPerBoxAmmount + " per box. Cost " + OverAmmoCost + " WP per box." + Environment.NewLine;
                    }
                    else
                    {
                        switch (OverLoadingName)// Increases the cost if you get over or underloaded ammo
                        {
                            case "Low Pressure":
                                OverAmmoCost += 1;
                                break;
                            case "High Pressure":
                                OverAmmoCost += 1;
                                break;
                            case "Over Pressure":
                                OverAmmoCost += 2;
                                break;
                            default:

                                break;

                        }

                        ammostring += OverAmmotype + " | " + amountcount + " Box of " + OverLoadingName + " " + OverCategoryType + ", " + OverAmmoMan + " Brand. " + OverPerBoxAmmount + " per box. Cost " + OverAmmoCost.ToString() + " WP per box." + Environment.NewLine;
                    }
                }
                else
                {
                    if (OverAmmotype.Contains("Shotshell"))
                    {
                        ammostring += OverAmmotype + " | " + amountcount + " Boxes of " + OverCategoryType + ", " + OverAmmoMan + " Brand. " + OverPerBoxAmmount + " per box. Cost " + OverAmmoCost + " WP per box." + Environment.NewLine;
                    }
                    else
                    {
                        switch (OverLoadingName)// Increases the cost if you get over or underloaded ammo
                        {
                            case "Low Pressure":
                                OverAmmoCost += 1;
                                break;
                            case "High Pressure":
                                OverAmmoCost += 1;
                                break;
                            case "Over Pressure":
                                OverAmmoCost += 2;
                                break;
                            default:

                                break;
                        }
                        ammostring += OverAmmotype + " | " + amountcount + " Boxes of " + OverLoadingName + " " + OverCategoryType + ", " + OverAmmoMan + " Brand. " + OverPerBoxAmmount + " per box. Cost " + OverAmmoCost + " WP per box." + Environment.NewLine;
                    }
                }
                ammocount--;
            }

            switch (gnadelaunc)// This one with this big chunk generates Grenade launchers
            {
                case "None":
                    glcount = 0;
                    break;
                case "Very Low":
                    glcount = glroller.Next(0, 2);
                    break;
                case "Low":
                    glcount = glroller.Next(0, 3);
                    break;
                case "Medium":
                    glcount = glroller.Next(1, 4);
                    break;
                case "High":
                    glcount = glroller.Next(2, 5);
                    break;
                case "Very High":
                    glcount = glroller.Next(3, 6);
                    break;
                case "Extremely High":
                    glcount = glroller.Next(4, 10);
                    break;
                default:
                    glcount = 0;
                    break;
            }
            sqlite_cmd = sqlite_conn.CreateCommand();
            sqlite_cmd.CommandText = "SELECT * FROM GrenadeLauncherTable WHERE License LIKE '%" + SQLRestrictionInput + "%' ORDER BY RANDOM() LIMIT " + glcount.ToString();
            sqlite_datareader = sqlite_cmd.ExecuteReader();




            if (glcount != 0)
            {
                gllabel = "__________________________Grenade Launchers__________________________" + Environment.NewLine + Environment.NewLine;
            }
            else
            {
                gllabel = "";
            }
            glstring += gllabel;
            while (sqlite_datareader.Read())
            {
                // Print out the content of the text field:
                // System.Console.WriteLine("DEBUG Output: '" + sqlite_datareader["text"] + "'");
                //0 Weapon Name 1 Caliber 2 Magazine 3 Upgrade Slot 4 Range 5 ROF 6 Size 7 Weight 8Cost 9error range 10 license 11 Notes 12 categories(Don't show this one)
                object WeaponName = sqlite_datareader.GetValue(0);
                string Caliber = sqlite_datareader.GetString(1);
                string Magazine = sqlite_datareader.GetString(2);
                string UpgradeSlot = sqlite_datareader.GetString(3);
                string Range = sqlite_datareader.GetString(4);
                string ROF = sqlite_datareader.GetString(5);
                string size = sqlite_datareader.GetString(6);
                double Weight = sqlite_datareader.GetDouble(7);
                int Cost = sqlite_datareader.GetInt32(8);
                int Error = sqlite_datareader.GetInt32(9);
                string License = sqlite_datareader.GetString(10);
                string Notes = sqlite_datareader.GetString(11);

                switch (gnadelaunc)
                {
                    case "None":
                        amountcount = 0;
                        break;
                    case "Very Low":
                        amountcount = gunroller.Next(1, 3);
                        break;
                    case "Low":
                        amountcount = gunroller.Next(2, 3);
                        break;
                    case "Medium":
                        amountcount = gunroller.Next(2, 4);
                        break;
                    case "High":
                        amountcount = gunroller.Next(2, 4);
                        break;
                    case "Very High":
                        amountcount = gunroller.Next(2, 5);
                        break;
                    case "Extremely High":
                        amountcount = gunroller.Next(3, 5);
                        break;
                    default:
                        amountcount = 1;
                        break;
                }
                glstring += amountcount.ToString() + "x " + WeaponName + " | Caliber: " + Caliber + " | Magazine: " + Magazine + " | Upgrade Points: " + UpgradeSlot + " | Range: " + Range + " | ROF:" + ROF + " | Weight: " + Weight.ToString() + " | Cost: " + Cost.ToString() + " | Error Range: " + Error.ToString() + "% | License: " + License + " | Standard Equipment: " + Notes + " " + Environment.NewLine + Environment.NewLine;

            }
            switch (rocketlaunch)// This one with this big chunk generates Rocket launchers
            {
                case "None":
                    rocketcount = 0;
                    break;
                case "Very Low":
                    rocketcount = rocketroller.Next(0, 2);
                    break;
                case "Low":
                    rocketcount = rocketroller.Next(0, 3);
                    break;
                case "Medium":
                    rocketcount = rocketroller.Next(1, 4);
                    break;
                case "High":
                    rocketcount = rocketroller.Next(2, 5);
                    break;
                case "Very High":
                    rocketcount = rocketroller.Next(3, 6);
                    break;
                case "Extremely High":
                    rocketcount = rocketroller.Next(4, 10);
                    break;
                default:
                    rocketcount = 0;
                    break;
            }
            sqlite_cmd = sqlite_conn.CreateCommand();
            sqlite_cmd.CommandText = "SELECT * FROM RocketLauncherTable WHERE License LIKE '%" + SQLRestrictionInput + "%' ORDER BY RANDOM() LIMIT " + rocketcount.ToString();
            sqlite_datareader = sqlite_cmd.ExecuteReader();




            if (rocketcount != 0)
            {
                rocketlabel = "__________________________Rocket Launchers__________________________" + Environment.NewLine + Environment.NewLine;
            }
            else
            {
                rocketlabel = "";
            }
            rocketstring += rocketlabel;
            while (sqlite_datareader.Read())
            {
                // Print out the content of the text field:
                // System.Console.WriteLine("DEBUG Output: '" + sqlite_datareader["text"] + "'");
                //0 Weapon Name 1 Caliber 2 Magazine 3 Upgrade Slot 4 Range 5 ROF 6 Size 7 Weight 8Cost 9error range 10 license 11 Notes 12 categories(Don't show this one)
                object WeaponName = sqlite_datareader.GetValue(0);
                string Damage = sqlite_datareader.GetString(1);
                string Magazine = sqlite_datareader.GetString(2);
                string UpgradeSlot = sqlite_datareader.GetString(3);
                string Range = sqlite_datareader.GetString(4);
                string ROF = sqlite_datareader.GetString(5);
                string size = sqlite_datareader.GetString(6);
                double Weight = sqlite_datareader.GetDouble(7);
                int Cost = sqlite_datareader.GetInt32(8);
                int Error = sqlite_datareader.GetInt32(9);
                string License = sqlite_datareader.GetString(10);
                string Notes = sqlite_datareader.GetString(11);

                switch (rocketlaunch)
                {
                    case "None":
                        amountcount = 0;
                        break;
                    case "Very Low":
                        amountcount = gunroller.Next(1, 3);
                        break;
                    case "Low":
                        amountcount = gunroller.Next(2, 3);
                        break;
                    case "Medium":
                        amountcount = gunroller.Next(2, 4);
                        break;
                    case "High":
                        amountcount = gunroller.Next(2, 4);
                        break;
                    case "Very High":
                        amountcount = gunroller.Next(2, 5);
                        break;
                    case "Extremely High":
                        amountcount = gunroller.Next(3, 5);
                        break;
                    default:
                        amountcount = 1;
                        break;
                }
                rocketstring += amountcount.ToString() + "x " + WeaponName + " | Damage: " + Damage + " | Magazine: " + Magazine + " | Upgrade Points: " + UpgradeSlot + " | Range: " + Range + " | ROF:" + ROF  + " | Size: " + size + " | Weight: " + Weight.ToString() + " | Cost: " + Cost.ToString() + " | Error Range: " + Error.ToString() + "% | License: " + License + " | Standard Equipment: " + Notes + " " + Environment.NewLine + Environment.NewLine;

            }


            gunstoreoutput.Text = handgunstring + shotgunstring + riflestring + pdwstring + mgstring + glstring + rocketstring + ammostring;



        }
    }
}