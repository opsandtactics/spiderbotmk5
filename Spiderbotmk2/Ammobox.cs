﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public class AmmoBox
    {

    public string Name { get; set; }
    public int Cost { get; set; }
    public int BoxAmount { get; set; }
    public string License { get; set; }


    public AmmoBox(string v1,  int v2, int v3, string v4)
    {
        this.Name = v1;
        this.Cost = v2;
        this.BoxAmount = v3;
        this.License = v4;
    }
}

