﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

   public class Items
    {
    public string Name { get; set; }
    public string Description { get; set; }
    public string Size { get; set; }
    public int Weight { get; set; }
    public double Cost { get; set; }
    public string Material { get; set; }

    public override string ToString()
    {
        System.Text.StringBuilder conversion = new System.Text.StringBuilder();
        conversion.Append(Material + " "+ Name + " |" + " Size:" + Size + " Weight:" + Weight.ToString() + "oz" + " Cost:" + Cost.ToString());
        return conversion.ToString();
    }
}

